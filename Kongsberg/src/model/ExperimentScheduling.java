package model;

import java.io.BufferedWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;

import database.DatabaseConnection;
import database.GetTestCasesQueries;
import simula.oclga.Search;
import view.ListTestCasesPage;

public class ExperimentScheduling {

	private DecimalFormat df;
	private BufferedWriter file;
	private int loopNum;
	private ProblemScheduling problemScheduling;
	private ProblemSchedulingSimple problemSchedulingSimple;
	private int jobsMax;
	private int jobsMin;
	private double maxTime;
	private double total;
	public double getTotal() {
		return total;
	}



	public void setTotal(double total) {
		this.total = total;
	}

	private double priority;
	private double probability;
	private double consequence;
	
	private String context;
	public String getContext() {
		return context;
	}



	public void setContext(String context) {
		this.context = context;
	}



	public String getComponent() {
		return component;
	}



	public void setComponent(String component) {
		this.component = component;
	}



	public String getConstraint() {
		return constraint;
	}



	public void setConstraint(String constraint) {
		this.constraint = constraint;
	}



	public String getEffect() {
		return effect;
	}



	public void setEffect(String effect) {
		this.effect = effect;
	}

	private String component;
	private String constraint;
	private String effect;


	public double getPriority() {
		return priority;
	}



	public void setPriority(double priority) {
		this.priority = priority;
	}

	public double getProbability() {
		return probability;
	}

	public void setProbability(double probability) {
		this.probability = probability;
	}

	public double getConsequence() {
		return consequence;
	}

	public void setConsequence(double consequence) {
		this.consequence = consequence;
	}

	private DatabaseConnection databaseConnection;
	
	private ArrayList<TestCase> testCaseList;
	private ListTestCasesPage listTestCasesPage;
	
	public DatabaseConnection getDatabaseConnection() {
		return databaseConnection;
	}

	public void setDatabaseConnection(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	private GetTestCasesQueries testCasesQueries;
	
	
	public ExperimentScheduling(){
		df = new DecimalFormat("0.000");
		loopNum = 100;
		
		listTestCasesPage = new ListTestCasesPage();
		testCasesQueries = new GetTestCasesQueries();
	}
	
	
	public ArrayList<TestCase> getValues_1() throws Exception {
		Search[] s = new Search[] { new simula.oclga.AVM(),
				new simula.oclga.SSGA(100, 0.75), new simula.oclga.OpOEA(),
				new simula.oclga.RandomSearch() };
		String[] s_name = new String[] { "(1+1)EA" };
		ArrayList<TestCase> tempCaseList = new ArrayList<TestCase>();
		int counter=0;
		double fitnessValue=1;
		
		for (int sea = 0; sea < 1; sea++) {
			for (int K = 0; K < 100; K++) {	
				problemScheduling = new ProblemScheduling();
				problemScheduling.setTestCaseList(testCaseList);
				problemScheduling.setJobsMax(jobsMax);
				problemScheduling.setJobsMin(jobsMin);
				problemScheduling.setTimeBudget(maxTime);
				problemScheduling.setPriority(priority);
				problemScheduling.setProbability(probability);
				problemScheduling.setConsequence(consequence);
				problemScheduling.setMax(counter);
					
				s[sea].setMaxIterations(2000);
				Search.validateConstraints(problemScheduling);
				int[] v_1 = s[sea].search(problemScheduling);
					
				//System.out.println("max is " + K + " "+ problemScheduling.getMax());
					
				
				if (counter<=problemScheduling.getMax()){
					counter=problemScheduling.getMax();
					//System.out.println("counter is  is " + K+ " " +counter);
				}
				if (fitnessValue>problemScheduling.getInitalFitnessValue()){
				//	System.out.println("fitness value2222222222222 is " +problemScheduling.getInitalFitnessValue());
					int size = tempCaseList.size()-1;
					while (size>=0){
					//	System.out.println(tempCaseList.get(size).getId());
						size--;
					}
					tempCaseList= problemScheduling.caseList;
					fitnessValue = problemScheduling.getInitalFitnessValue();
					}
				}			
					//file.write(df.format(m) + "\t"); //		
			//	file.write("\r");
			//	file.flush();
			}
		
		return tempCaseList;
	}
	
	public ArrayList<TestCase> getValues_2() throws Exception {
		Search[] s = new Search[] { new simula.oclga.AVM(),
				new simula.oclga.SSGA(100, 0.75), new simula.oclga.OpOEA(),
				new simula.oclga.RandomSearch() };
		String[] s_name = new String[] { "(1+1)EA" };
		ArrayList<TestCase> tempCaseList = new ArrayList<TestCase>();
		int counter=0;
		double fitnessValue=1;
		for (int sea = 0; sea < 1; sea++) {
			for (int K = 0; K < 100; K++) {	
				problemSchedulingSimple = new ProblemSchedulingSimple();
			
				problemSchedulingSimple.setTestCaseList(testCaseList);
				problemSchedulingSimple.setJobsMax(jobsMax);
				problemSchedulingSimple.setJobsMin(jobsMin);
				problemSchedulingSimple.setTimeBudget(maxTime);			
				problemSchedulingSimple.calculate();
	
				s[sea].setMaxIterations(1000);
				Search.validateConstraints(problemSchedulingSimple);
				int[] v_1 = s[sea].search(problemSchedulingSimple);
	
			
				if (fitnessValue>problemSchedulingSimple.getInitalFitnessValue()){
					int size = tempCaseList.size()-1;
					while (size>=0){
					//	System.out.println(tempCaseList.get(size).getId());
						size--;
					}
					tempCaseList= problemSchedulingSimple.caseList;
					fitnessValue = problemSchedulingSimple.getInitalFitnessValue();
				}
			}
		}
		return tempCaseList;
	}
	

	public int getJobsMin() {
		return jobsMin;
	}

	public void setJobsMin(int jobsMin) {
		this.jobsMin = jobsMin;
	}

	public int getJobsMax() {
		return jobsMax;
	}

	public void setJobsMax(int jobsMax) {
		this.jobsMax = jobsMax;
	}

	public double getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(double maxTime) {
		this.maxTime = maxTime;
	}
	public 	ArrayList<TestCase> run(){
		ArrayList<TestCase> tempCaseList = new ArrayList<TestCase>();
		getTestCases();
		if (this.total>0){
			try {
				tempCaseList= getValues_1();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if (this.total==0 && this.maxTime!=0)
			try {
				tempCaseList= getValues_2();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else	
			tempCaseList= this.testCaseList;
		return tempCaseList;
		
	}
	
	
	public void getTestCases(){
		testCasesQueries.setDatabaseConnection(databaseConnection);
		testCaseList= testCasesQueries.getTestCaseContents(context, component, constraint, effect);
		
		jobsMax = testCaseList.size();
		jobsMin = 0;
	}
	
}
