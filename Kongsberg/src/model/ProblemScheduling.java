package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import simula.oclga.Problem;



public class ProblemScheduling implements Problem{
	public ArrayList<TestCase> caseList;
	int counter;
	int[][] values;
	public double initalFitnessValue;
	
	public double getInitalFitnessValue() {
		return initalFitnessValue;
	}

	public void setInitalFitnessValue(double initalFitnessValue) {
		this.initalFitnessValue = initalFitnessValue;
	}

	private double timeBudget;
	private double priority;
	private double probability;
	private double consequence;
	private int max;
	private double tm = 0;
	private String context;
	
	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public double getTm() {
		return tm;
	}

	public void setTm(double tm) {
		this.tm = tm;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public double getPriority() {
		return priority;
	}

	public void setPriority(double priority) {
		this.priority = priority;
	}

	public double getProbability() {
		return probability;
	}

	public void setProbability(double probability) {
		this.probability = probability;
	}

	public double getConsequence() {
		return consequence;
	}

	public void setConsequence(double consequence) {
		this.consequence = consequence;
	}

	public double getTimeBudget() {
		return timeBudget;
	}

	public void setTimeBudget(double timeBudget) {
		this.timeBudget = timeBudget;
	}

	private int jobsMax;
	private ArrayList<TestCase> testCaseList;
	
	public int getJobsMax() {
		return jobsMax;
	}

	public void setJobsMax(int jobsMax) {
		this.jobsMax = jobsMax;
	}

	public int getJobsMin() {
		return jobsMin;
	}

	public void setJobsMin(int jobsMin) {
		this.jobsMin = jobsMin;
	}

	private int jobsMin;
	
	
	public ProblemScheduling(){
		caseList = new ArrayList<TestCase>();
		initalFitnessValue =1;
		
	}
	
	
	public double getFitness(int[] v) { //it would be better to rerun a list or String[], the first value is fitness function, the others are the jobid of selected test cases.	
		ArrayList<TestCase> tempCaseList = new ArrayList<TestCase>();
		
		ArrayList<TestCase> tempCase2 = new ArrayList<TestCase>();
		
		int count =0;
		int countTemp2=0;
		double tempTime2=0;
		double temptime =0;
		double priorityValue=0;
		double probabilityValue=0;
		double consequenceValue =0;
		double prioritySum =0;
		double probabilitySum =0;
		double consequenceSum =0;
		double overallProbability=0;
		double overallPriority=0;
		double overallConsequence=0;
	
		
		HashSet<Integer> tempset = new HashSet<Integer>();
		int k=0;
		for (int i=0;i<testCaseList.size();i++){
		
			if (tempCase2.size()!=testCaseList.size()){
				if (v[i]==1){
					tempset.add(i);
					tempCase2.add(testCaseList.get(k));
					
				}
				k++;
			}		
		}
		// calculating fitness function values
		for (int j=0;j<tempCase2.size();j++){
			Priority priorityTemp = new Priority();
			Probability probabilityTemp = new Probability();	
			TestCase tempCase = tempCase2.get(j);		
			temptime +=tempCase.getTimeExecution();
			priorityTemp = tempCase.getPriority();
			probabilityTemp = tempCase.getProbability();

			if (priorityTemp.getName().equalsIgnoreCase("higher")||priorityTemp.getName().equalsIgnoreCase("high")||priorityTemp.getName().equalsIgnoreCase("medium")
					||priorityTemp.getName().equalsIgnoreCase("low")||priorityTemp.getName().equalsIgnoreCase("lower"))
				priorityValue= convertPriority(priorityTemp);
			else
				priorityValue=0;
			
			if (probabilityTemp.getName().equalsIgnoreCase("high") || probabilityTemp.getName().equalsIgnoreCase("medium") || probabilityTemp.getName().equalsIgnoreCase("low"))
				probabilityValue= convertProbability(probabilityTemp);

			else
				probabilityValue=0;
				
			count++;
			tempCaseList.add(tempCase);
			prioritySum+=priorityValue;
			probabilitySum+=probabilityValue;

		}
		overallPriority=prioritySum/count;
		overallProbability=probabilitySum/count;
		double time =0;

		time = 1-Nor(Math.abs(temptime-timeBudget));
		
		//calculate fitness function
		tm =1- (priority*overallPriority + probability*overallProbability + 0.3*time)/1.3;

		if (initalFitnessValue>tm && count != 0 ){
			initalFitnessValue=tm;
			caseList=tempCaseList;			
		}		
		counter++;
		return tm;
		
	}
	
	
	public double convertPriority(Priority priority){
		double priorityNum=0;
		if (priority.getName().equalsIgnoreCase("higher"))
			priorityNum=1;
		else if (priority.getName().equalsIgnoreCase("high"))
			priorityNum=0.8;
		else if (priority.getName().equalsIgnoreCase("medium"))
			priorityNum=0.6;
		else if (priority.getName().equalsIgnoreCase("low"))
			priorityNum=0.4;
		else if (priority.getName().equalsIgnoreCase("lower"))
			priorityNum=0.2;
		
		return priorityNum;
	}
	
	public double convertProbability(Probability probability){
		double priorityNum=0;
		if (probability.getName().equalsIgnoreCase("high"))
			priorityNum=1;
		else if (probability.getName().equalsIgnoreCase("medium"))
			priorityNum=0.66;
		else if (probability.getName().equalsIgnoreCase("low"))
			priorityNum=0.33;
		
		
		return priorityNum;
	}
	
	public double convertConsequence(Consequence consequence){
		double consequenceNum=0;
		if (consequence.getName().equalsIgnoreCase("higher"))
			consequenceNum=2;
		else if (consequence.getName().equalsIgnoreCase("high"))
			consequenceNum=0.8;
		else if (consequence.getName().equalsIgnoreCase("medium"))
			consequenceNum=0.6;
		else if (consequence.getName().equalsIgnoreCase("low"))
			consequenceNum=0.4;
		else if (consequence.getName().equalsIgnoreCase("lower"))
			consequenceNum=0.2;
		
		return consequenceNum;
	}

	// normalization function
	public double Nor(double n) {
		double m = n / (n + 1);
		return m;
	}

	@Override
	public int[][] getConstraints() {
		// TODO Auto-generated method stub
		int valuesOfConstraints[][] = new int[testCaseList.size()][3];
		
		for (int i=0;i<testCaseList.size();i++){
			valuesOfConstraints[i][0] = 0;
			valuesOfConstraints[i][1] = 1;
			valuesOfConstraints[i][2] = 0;			
		}
		setValues(valuesOfConstraints);
		return values;
	}
	
	
	public void setValues(int[][] values) {
		this.values = values;
	}

	public ArrayList<TestCase> getTestCaseList() {
		return testCaseList;
	}

	public void setTestCaseList(ArrayList<TestCase> testCaseList) {
		this.testCaseList = testCaseList;
	}
	
}
//	
//	@Override
//	public double getFitness(int[] v) { //it would be better to rerun a list or String[], the first value is fitness function, the others are the jobid of selected test cases.
//		
//		ArrayList<TestCase> tempCaseList = new ArrayList<TestCase>();
//		
//		int count =0;
//		int countTemp2=0;
//		double tempTime2=0;
//		 // fitness value
//		double temptime =0;
//		double priorityValue=0;
//		double probabilityValue=0;
//		double consequenceValue =0;
//		double prioritySum =0;
//		double probabilitySum =0;
//		double consequenceSum =0;
//		double overallProbability=0;
//		double overallPriority=0;
//		double overallConsequence=0;
//
//		ArrayList<TestCase> tempjobs = new ArrayList<TestCase>();
//		// get the potential solution
//		HashSet<Integer> tempset = new HashSet<Integer>();
//		
//		if (v[0]>=jobsMax)
//			v[0] = jobsMax-1;
//		while (tempset.size()<v[0]){ // v[0] is like 200 test cases for solution
//			tempset.add((int)(Math.random() * ((jobsMax)-1) + 1));
//		}
//		Iterator<Integer> it = tempset.iterator();		
//		// calculating fitness function values
//		while (it.hasNext()){			
//			Priority priorityTemp = new Priority();
//			Probability probabilityTemp = new Probability();
//			Consequence consequenceTemp = new Consequence();
//			
//			TestCase tempCase = testCaseList.get(it.next());
//			int maxuploadcost = 0;
//			int maxsubnetcost = 0;
//						
//			double sumweight = 0;			
//			temptime +=tempCase.getTimeExecution();
//			priorityTemp = tempCase.getPriority();
//			probabilityTemp = tempCase.getProbability();
//			consequenceTemp = tempCase.getConsequence();
//			priorityValue= convertPriority(priorityTemp);
//			probabilityValue= convertProbability(probabilityTemp);
//			consequenceValue= convertConsequence(consequenceTemp);
//			
//			tempTime2 +=tempCase.getTimeExecution();
//			if (tempTime2>timeBudget)
//				tempTime2 -=tempCase.getTimeExecution();				
//			else
//				countTemp2++;
//
//			count++;
//			tempCaseList.add(tempCase);
//			prioritySum+=priorityValue;
//			probabilitySum+=probabilityValue;
//			consequenceSum+=consequenceValue;
//				
//		//	}
//		}
//	
//		if (max<countTemp2){
//		//	System.out.println("maximum valueee  is " + max);
//			max=countTemp2;
//		}
//		
//		if (temptime>timeBudget)
//			return 1;
//			else {
//		if (max>=count){
//			overallPriority=prioritySum/max;
//			overallProbability=probabilitySum/max;
//			overallConsequence=consequenceSum/max;
//		}else{
//			overallPriority=prioritySum/count;
//			overallProbability=probabilitySum/count;
//			overallConsequence=consequenceSum/count;
//			
//		}
////		if (temptime>timeBudget)
////			return 1;
////		else {
////		double timeTemp2 = temptime-timeBudget;
//		double time =0;
////		if (timeTemp2<0)
////			time = 1- Nor(Math.abs(timeTemp2));
////		else
//			time = 1-Nor(Math.abs(temptime-timeBudget));
//		
//			//calculate fitness function
//			tm =1- (priority*overallPriority + probability*overallProbability + consequence*overallConsequence)/1;
//			if (tm<0.1){
////				System.out.println("time  is " + time);
////				System.out.println("tmmmmmmmmm  is " + tm);
//			}
//				
//			if (initalFitnessValue>tm & count != 0 ){
//				initalFitnessValue=tm;
//				caseList=tempCaseList;		
//				//System.out.println("tmmmmmmmmm  is " + tm);
//			}
//			
//			counter++;
//			return tm;
//		}
//		}		

