package model;

public class TestCase {
	
	private int id;
	private Priority priority;
	private Probability probability;
	private Effect effect;
	private Context context;
	private String caseName;
	private String goal;
	private Model model;
	private RCUType rcuType;
	private ModelConstraint modelConstraint;
	private double timeExecution;
	private Consequence consequence;
	
	

	public Consequence getConsequence() {
		return consequence;
	}

	public void setConsequence(Consequence consequence) {
		this.consequence = consequence;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public Probability getProbability() {
		return probability;
	}

	public void setProbability(Probability probability) {
		this.probability = probability;
	}

	public Effect getEffect() {
		return effect;
	}

	public void setEffect(Effect effect) {
		this.effect = effect;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public String getCaseName() {
		return caseName;
	}

	public void setCaseName(String caseName) {
		this.caseName = caseName;
	}

	public String getGoal() {
		return goal;
	}

	public void setGoal(String goal) {
		this.goal = goal;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public RCUType getRcuType() {
		return rcuType;
	}

	public void setRcuType(RCUType rcuType) {
		this.rcuType = rcuType;
	}

	public ModelConstraint getModelConstraint() {
		return modelConstraint;
	}

	public void setModelConstraint(ModelConstraint modelConstraint) {
		this.modelConstraint = modelConstraint;
	}

	public double getTimeExecution() {
		return timeExecution;
	}

	public void setTimeExecution(double timeExecution) {
		this.timeExecution = timeExecution;
	}

}
