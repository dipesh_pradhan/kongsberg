package view;

import java.awt.EventQueue;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;

import controller.MainPageController;
import model.Context;
import model.Effect;
import model.ExperimentScheduling;
import model.ModelConstraint;
import model.Priority;
import model.Probability;
import model.RCUType;
import model.TestCase;
import database.ContextQueries;
import database.CreateTestCaseQueries;
import database.DatabaseConnection;
import database.EffectQueries;
import database.ModelConstraintQueries;
import database.ModelQueries;
import database.OptimizeQueries;
import database.PriorityQueries;
import database.ProbabilityQueries;
import database.RCUTypeQueries;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Iterator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import javax.swing.JSlider;

public class TestCaseSelectionPage {

	public DatabaseConnection databaseConnection;
	
	public JFrame frame;
	private JTextField textTime;
	
	
	public PriorityQueries priorityQueries;
	public ProbabilityQueries probabilityQueries;
	public EffectQueries effectQueries;
	public ContextQueries contextQueries;
	private ModelQueries modelQueries;	
	private ModelConstraintQueries modelConstraintQueries;
	private RCUTypeQueries rcuTypeQueries;
	private OptimizeQueries optimizeQueries;
	
	private JButton btnCancel, btnDefaultConfiguration, btnOptimizeCases, btnOptimize;
	private JComboBox comboBoxEffect, comboBoxContext, comboBoxModel, comboBoxComponent, comboBoxModelConstraint;
	private JSlider sliderPriority, sliderProbability, sliderConsequence;
	
	final int MIN = 0;
	final int MAX = 100;
	final int FPS_INIT = 15; 
	
	private MainPageController mainPageController;
	private SelectionTestCasesPage selectionTestCasesPage;
	private ListTestCasesPage listTestCasesPage;
	private ExperimentScheduling experimentScheduling;
	private JLabel lblConsequence;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestCaseSelectionPage window = new TestCaseSelectionPage();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TestCaseSelectionPage() {
		initComponents();
		
		databaseConnection.connect();
		initialize();
		
		this.fillComboBoxes();
		setUpListeners();
	}
	

	public void initComponents(){
		databaseConnection = new DatabaseConnection();
		priorityQueries = new PriorityQueries();
		probabilityQueries = new ProbabilityQueries();
		effectQueries = new EffectQueries();
		contextQueries = new ContextQueries();
		modelQueries = new ModelQueries();
		rcuTypeQueries = new RCUTypeQueries();
		modelConstraintQueries = new ModelConstraintQueries();
		optimizeQueries = new OptimizeQueries();
		
		mainPageController = new MainPageController();
	//	selectionTestCasesPage = new SelectionTestCasesPage();
		listTestCasesPage = new ListTestCasesPage();
		experimentScheduling = new ExperimentScheduling();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 906, 556);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Test Case Selection");
		
		JLabel lblExecutionTime = new JLabel("Execution Time");
		lblExecutionTime.setBounds(59, 70, 101, 14);
		frame.getContentPane().add(lblExecutionTime);
		
		textTime = new JTextField();
		textTime.setBounds(210, 67, 180, 20);
		frame.getContentPane().add(textTime);
		textTime.setColumns(10);
		
		JLabel lblHours = new JLabel("hours");
		lblHours.setBounds(400, 70, 46, 14);
		frame.getContentPane().add(lblHours);
		
		JLabel lblSelectTestCases = new JLabel("Select test cases based on the following choices:");
		lblSelectTestCases.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSelectTestCases.setBounds(59, 20, 375, 14);
		frame.getContentPane().add(lblSelectTestCases);
		
		JLabel label = new JLabel("Context");
		label.setBounds(59, 122, 46, 14);
		frame.getContentPane().add(label);
		
		comboBoxContext = new JComboBox();
		
		comboBoxContext.setBounds(213, 119, 172, 20);
		frame.getContentPane().add(comboBoxContext);
		
		JLabel label_1 = new JLabel("Priority");
		label_1.setBounds(59, 178, 46, 14);
		frame.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("Probability");
		label_2.setBounds(59, 239, 75, 14);
		frame.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("Component Under Test");
		label_3.setBounds(448, 163, 140, 14);
		frame.getContentPane().add(label_3);
		
		comboBoxComponent = new JComboBox();
		comboBoxComponent.setBounds(596, 160, 175, 20);
		frame.getContentPane().add(comboBoxComponent);
		
		JLabel label_4 = new JLabel("Feature");
		label_4.setBounds(448, 239, 97, 14);
		frame.getContentPane().add(label_4);
		
		comboBoxModelConstraint = new JComboBox();
		comboBoxModelConstraint.setBounds(596, 236, 175, 20);
		frame.getContentPane().add(comboBoxModelConstraint);
		
		btnCancel = new JButton("Cancel");
		
		btnCancel.setBounds(221, 426, 120, 23);
		frame.getContentPane().add(btnCancel);
        Font font = new Font("Serif", Font.ITALIC, 15);
		
		sliderPriority = new JSlider(JSlider.HORIZONTAL,
                MIN, MAX, FPS_INIT);
		sliderPriority.setBounds(199, 152, 200, 77);
		frame.getContentPane().add(sliderPriority);
		
		sliderPriority.setMajorTickSpacing(25);
		sliderPriority.setMinorTickSpacing(5);
		sliderPriority.setPaintTicks(true);
        sliderPriority.setPaintLabels(true);
        sliderPriority.setBorder(
                BorderFactory.createEmptyBorder(0,0,10,0));
        sliderPriority.setFont(font);
		
		sliderProbability = new JSlider(JSlider.HORIZONTAL,
                MIN, MAX, FPS_INIT);
		sliderProbability.setBounds(199, 219, 200, 85);
		frame.getContentPane().add(sliderProbability);
				
		sliderProbability.setMajorTickSpacing(25);
		sliderProbability.setMinorTickSpacing(5);
		sliderProbability.setPaintTicks(true);
        sliderProbability.setPaintLabels(true);
        sliderProbability.setBorder(
                BorderFactory.createEmptyBorder(0,0,10,0));
        sliderProbability.setFont(font);
        
		btnDefaultConfiguration = new JButton("Default configuration");
		btnDefaultConfiguration.setBounds(400, 426, 175, 23);
		frame.getContentPane().add(btnDefaultConfiguration);
		
		JLabel lblEffect = new JLabel("Type of Tests");
		lblEffect.setBounds(448, 309, 127, 14);
		frame.getContentPane().add(lblEffect);
		
		comboBoxEffect = new JComboBox();
		comboBoxEffect.setBounds(596, 306, 175, 20);
		frame.getContentPane().add(comboBoxEffect);
		
		lblConsequence = new JLabel("Consequence");
		lblConsequence.setBounds(59, 309, 95, 14);
		//frame.getContentPane().add(lblConsequence);
		
		sliderConsequence = new JSlider(JSlider.HORIZONTAL,
                MIN, MAX, FPS_INIT);
		sliderConsequence.setBounds(199, 293, 200, 96);
		//frame.getContentPane().add(sliderConsequence);
		
		sliderConsequence.setMajorTickSpacing(25);
		sliderConsequence.setMinorTickSpacing(5);
		sliderConsequence.setPaintTicks(true);
		sliderConsequence.setPaintLabels(true);
		sliderConsequence.setBorder(
                BorderFactory.createEmptyBorder(0,0,10,0));
		sliderConsequence.setFont(font);
		
		btnOptimizeCases = new JButton("Optimize cases");
		btnOptimizeCases.setBounds(631, 426, 140, 23);
		//frame.getContentPane().add(btnOptimizeCases);
		
		btnOptimize = new JButton("Optimize");
		btnOptimize.setBounds(40, 426, 120, 23);
		frame.getContentPane().add(btnOptimize);
		
		
	}
	
	public void fillComboBoxes(){
		
		try{
			this.contextQueries.addContext(databaseConnection);
			Iterator<Context> contextIterator = this.contextQueries.getContext().iterator();
			this.comboBoxContext.addItem("");
			while (contextIterator.hasNext()){
				this.comboBoxContext.addItem(contextIterator.next().getName());
				contextIterator.remove();
			}			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			this.rcuTypeQueries.addRCUType(databaseConnection);
			Iterator<RCUType> rcuTypeIterator = this.rcuTypeQueries.getRCUType().iterator();
			this.comboBoxComponent.addItem("");
			while (rcuTypeIterator.hasNext()){
				this.comboBoxComponent.addItem(rcuTypeIterator.next().getName());
				rcuTypeIterator.remove();
			}			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			this.modelConstraintQueries.addModelConstraint(databaseConnection);
			Iterator<ModelConstraint> modelConstraintIterator = this.modelConstraintQueries.getModelConstraint().iterator();
			this.comboBoxModelConstraint.addItem("");
			while (modelConstraintIterator.hasNext()){
				this.comboBoxModelConstraint.addItem(modelConstraintIterator.next().getName());
				modelConstraintIterator.remove();
			}			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			this.effectQueries.addEffect(databaseConnection);
			Iterator<Effect> effectIterator = this.effectQueries.getEffect().iterator();
			this.comboBoxEffect.addItem("");
			while (effectIterator.hasNext()){
				this.comboBoxEffect.addItem(effectIterator.next().getName());
				effectIterator.remove();
			}			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void setUpListeners(){
	
//		comboBoxContext.addActionListener (new ActionListener () {
//		    public void actionPerformed(ActionEvent e) {
//		    	frame.dispose();
//		    	System.out.println(comboBoxContext.getSelectedItem().toString());
//		    	listTestCasesPage.listContextCases(comboBoxContext.getSelectedItem().toString());
//		    	listTestCasesPage.frame.setVisible(true);
//		    }
//		});
//		
//		comboBoxComponent.addActionListener (new ActionListener () {
//		    public void actionPerformed(ActionEvent e) {
//		    	frame.dispose();
//		    	listTestCasesPage.listComponentCases(comboBoxComponent.getSelectedItem().toString());
//		    	listTestCasesPage.frame.setVisible(true);
//		    }
//		});
//		
//		comboBoxModelConstraint.addActionListener (new ActionListener () {
//		    public void actionPerformed(ActionEvent e) {
//		    	frame.dispose();
//		    	System.out.println(comboBoxModelConstraint.getSelectedItem().toString());
//		    	listTestCasesPage.listModelConstraintCases(comboBoxModelConstraint.getSelectedItem().toString());
//		    	listTestCasesPage.frame.setVisible(true);
//		    }
//		});
		
		btnOptimize.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double time, priority, probability, consequence;
				String context, component, constraint, effect;
				
				context =comboBoxContext.getSelectedItem().toString();
				component =comboBoxComponent.getSelectedItem().toString();
				constraint =comboBoxModelConstraint.getSelectedItem().toString();
				effect =comboBoxEffect.getSelectedItem().toString();
				
				
				if (textTime.getText().equals(""))
						time = 0;
				else
					time = Integer.parseInt(textTime.getText());
			//	context = sliderContext.getValue();
				priority = sliderPriority.getValue();
				probability = sliderProbability.getValue();
				consequence = sliderConsequence.getValue();
				
				double total = priority + probability;
				priority = priority/total;
				probability = probability/total;
	
				
				
				ArrayList<TestCase> tempCaseList = new ArrayList<TestCase>();
				frame.dispose();
				experimentScheduling.setDatabaseConnection(databaseConnection);
				experimentScheduling.setMaxTime(time);
				//experimentScheduling.setContext(context);
				experimentScheduling.setPriority(priority);
				experimentScheduling.setProbability(probability);
				//experimentScheduling.setConsequence(consequence);
				experimentScheduling.setContext(context);
				experimentScheduling.setComponent(component);
				experimentScheduling.setConstraint(constraint);
				experimentScheduling.setEffect(effect);
				experimentScheduling.setTotal(total);
	
				tempCaseList=experimentScheduling.run();
				
				listTestCasesPage.listCostCases(tempCaseList);
				listTestCasesPage.frame.setVisible(true);
			}
		});
		
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				mainPageController.runMainPage();
				
			}
		});
		
		btnDefaultConfiguration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			//	sliderContext.setValue(50);
				sliderPriority.setValue(50);
				sliderProbability.setValue(50);
				//sliderConsequence.setValue(0);
						
				textTime.setText("40");
			}
		});
		
		btnOptimizeCases.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				optimizeQueries.calculate(databaseConnection);
				
			}
		});
	}
}
